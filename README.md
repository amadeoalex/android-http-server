# DISCLAIMER
This app was created "committed" relatively long time ago. It is full of bad practices and now it serves no other purpose rather than show that I am actually making progress ;).
Please **DO NOT** follow the deign patterns used in this project

# Android HTTP Server

This is a very basic implementation of HTTP server (1.1). Created for personal use - to host static page on spare, not used Android phone.

## Project status

 - [x] Dead

## Installation

Currently to use tha app you have to download the sources and build it yourself. Although it works and does what is needs to do, at this stage this application is nowhere near being ready to publish anywhere in apk form.

## Build With

* [Android Studio](https://developer.android.com/studio/)
* [Gradle](https://gradle.org/)

## Authors

Paweł Wojtczak

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details

## Note

This application was inspired by one of the streams of Gynvael Coldwind during which he created even simpler implementation of HTTP server with Python. Sole purpose of this application was ~~(and probably still is)~~ to host my static webpage.
