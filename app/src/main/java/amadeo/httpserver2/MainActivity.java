package amadeo.httpserver2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Date;

import amadeo.httpserver2.service.ForegroundService;
import amadeo.httpserver2.service.server.ServerThread;

public class MainActivity extends AppCompatActivity implements ConnectionCallback {
    private final String TAG = "HTTPServer MA";

    private TextView ipTextView;
    private TextView logTextView;
    private ScrollView logScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ipTextView = findViewById(R.id.ipTextView);
        logTextView = findViewById(R.id.logTextView);
        logScrollView = findViewById(R.id.logScrollView);

        ipTextView.setText(NetworkUtils.getIPAddress());

        Intent intent = getIntent();
        if (intent != null)
            handleIntent(intent);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        ForegroundService.CreateWork(this, service -> {
            boolean stopped = service.serverThread == null || !service.serverThread.isAlive();

            menu.findItem(R.id.menu_main_start_server).setEnabled(stopped);
            menu.findItem(R.id.menu_main_stop_server).setEnabled(!stopped);
        });

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        ForegroundService.CreateWork(this, service -> {
            switch (item.getItemId()) {
                case R.id.menu_main_start_server:
                    //just to be sure
                    if (service.serverThread != null && service.serverThread.isAlive()) {
                        log(TAG, "this 'should not' happen");
                        break;
                    }

                    service.serverThread = new ServerThread(MainActivity.this);
                    service.serverThread.start();

                    break;
                case R.id.menu_main_stop_server:
                    //just to be sure
                    if (service.serverThread != null && !service.serverThread.isAlive()) {
                        log(TAG, "this 'should not' happen");
                        break;
                    }

                    if (service.serverThread != null)
                        service.serverThread.interrupt();
                case R.id.menu_main_parse_posts:
                    Intent intent = new Intent();
                    intent.setAction("amadeo.intent.action.NEW_POSTS");
                    sendBroadcast(intent);
                    break;
            }
        });

        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        String action = intent.getAction();
        if (action != null) {
            Log.i(TAG, "received new intent: " + intent.getAction());

            switch (action) {
                case "amadeo.intent.CONTENT":
                    break;
                case "amadeo.intent.STOP":
                    ForegroundService.CreateWork(getApplicationContext(), service -> {
                        service.stopForeground(true);
                        service.stopSelf();
                    });
                    finish();
                    break;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        ForegroundService.CreateWork(this, service -> service.activityConnectionCallback = MainActivity.this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ForegroundService.CreateWork(this, service -> service.activityConnectionCallback = null);
    }

    @Override
    public void onConnected() {

    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void log(final String TAG, final String message, final Object... objects) {
        runOnUiThread(() -> {
            if (message == null)
                return;

            Date date = new Date(System.currentTimeMillis());
            String timeStamp = "[" + DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT).format(date) + "]";

            String formattedMessage = String.format(message, objects);
            String log = timeStamp + "[" + TAG + "] " + formattedMessage + "\n";

            logTextView.append(log);
            logScrollView.fullScroll(View.FOCUS_DOWN);
        });

    }
}
