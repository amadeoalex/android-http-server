package amadeo.httpserver2;

import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Amadeo on 2018-07-21.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class HTML {
    public static final String HTML_FILES_PATH = "HTTP";
    public static final String ERROR_CODES_PATH = HTML_FILES_PATH + "/errorcodes";
    //HTTP/1.1 code codeMessage
    //Content-Type: mime
    //Content-Length: length
    public static final String HTTP_RESPONSE_TEMPLATE = "HTTP/1.1 %s %s\n" +
            "Content-Type: %s\n" +
            "Content-Length: %d\n" +
            "\r\n";

    public static final String HTTP_DEFAULT_RESPONSE_404 = "HTTP/1.1 404 Not Found\n" +
            "Content-Type: text/html;charset=utf-8\n" +
            "Content-Length: 15\n" +
            "\r\n" +
            "Sorry, nope.avi\n";
    public static final String HTTP_RESPONSE_404 = loadErrorResponse("404.html", HTTP_DEFAULT_RESPONSE_404);

    public static final String HTTP_DEFAULT_RESPONSE_501 = "HTTP/1.1 501 Not Implemented\n" +
            "Content-Type: text/html;charset=utf-8\n" +
            "Content-Length: 26\n" +
            "\r\n" +
            "Sorry, not supported ?yet?\n";
    public static final String HTTP_RESPONSE_501 = loadErrorResponse("501.html", HTTP_DEFAULT_RESPONSE_501);

    private static String loadErrorResponse(String fileName, String fallback) {
        String htmlFileContent = null;

        String filePath = Environment.getExternalStorageDirectory() + "/" + ERROR_CODES_PATH + "/" + fileName;
        File file = new File(filePath);

        if (file.exists() && !file.isDirectory()) {
            FileInputStream fileInputStream = null;
            Scanner scanner = null;
            try {
                fileInputStream = new FileInputStream(file);
                scanner = new Scanner(fileInputStream).useDelimiter("\\A");

                htmlFileContent = scanner.hasNext() ? scanner.next() : fallback;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (fileInputStream != null)
                        fileInputStream.close();
                    if (scanner != null)
                        scanner.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            htmlFileContent = fallback;
        }

        return htmlFileContent;
    }

    public static final Map<String, String> MIME_TYPES = Collections.unmodifiableMap(new HashMap<String, String>() {{
        put(".html", "text/html;charset=utf-8");
        put(".png", "image/png");
        put(".ico", "image/png");
        put(".css", "text/css");
    }});
}
