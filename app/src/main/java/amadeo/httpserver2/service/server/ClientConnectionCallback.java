package amadeo.httpserver2.service.server;

/**
 * Created by Amadeo on 2018-07-18.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public interface ClientConnectionCallback {
    void onConnected();

    void onDisconnected();

    void logClient(String TAG, String message, Object... objects);
}
