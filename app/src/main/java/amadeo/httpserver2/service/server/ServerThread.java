package amadeo.httpserver2.service.server;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import amadeo.httpserver2.ConnectionCallback;

/**
 * Created by Amadeo on 2018-07-18.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class ServerThread extends Thread implements ClientConnectionCallback {
    private final String TAG = "HTTPServer ST";
    private final String logTAG = "ServerThread";

    private ServerSocket serverSocket;
    private final ConnectionCallback connectionCallback;
    private final ThreadPoolExecutor executorService;

    private PowerManager.WakeLock wakeLock;

    private boolean running = true;

    public ServerThread(ConnectionCallback connectionCallback) {
        this.connectionCallback = connectionCallback;

        if (connectionCallback instanceof Context) {
            Context context = (Context) connectionCallback;

            PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            if (powerManager != null)
                wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "HTTPServer::MainWakeLock");
        } else {
            Log.w(TAG, "connectionCallback is not instance of context, wakelock disabled");
        }


        //should be enough for basic "nonexistent" traffic ;)
        executorService = new ThreadPoolExecutor(16, 16, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(16), new ThreadPoolExecutor.DiscardOldestPolicy());
    }

    @Override
    public void run() {
        log("server thread started");

        try {
            serverSocket = new ServerSocket(8080);

            while (running) {
                Socket socket = serverSocket.accept();

                ClientThread clientThread = new ClientThread(this, socket, wakeLock);
                executorService.execute(clientThread);

                log("%s connected, assigned id: %d", socket.getInetAddress().getHostAddress(), clientThread.getId());
            }
        } catch (IOException e) {
            if (running)
                e.printStackTrace();
        } finally {
            try {
                if (serverSocket != null && !serverSocket.isClosed())
                    serverSocket.close();

                executorService.shutdownNow();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        log("server thread stopped");
    }

    private void log(String message, Object... objects) {
        connectionCallback.log(logTAG, message, objects);
    }

    @Override
    public void onConnected() {

    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void logClient(String TAG, String message, Object... objects) {
        connectionCallback.log(TAG, message, objects);
    }

    @Override
    public void interrupt() {
        try {
            running = false;

            if (serverSocket != null && !serverSocket.isClosed())
                serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
