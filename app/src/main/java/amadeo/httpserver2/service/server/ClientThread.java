package amadeo.httpserver2.service.server;

import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Locale;

import amadeo.httpserver2.HTML;

/**
 * Created by Amadeo on 2018-07-18.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
class ClientThread extends Thread {
    private final String TAG = "HTTPServer CT";

    private final ClientConnectionCallback clientConnectionCallback;
    private final Socket socket;
    private final PowerManager.WakeLock wakeLock;
    private String threadId;

    private final String METHOD = "method";
    private final String HTTP_RESPONSE_TEMPLATE = HTML.HTTP_RESPONSE_TEMPLATE;

    private boolean running = true;

    public ClientThread(ClientConnectionCallback clientConnectionCallback, Socket socket, PowerManager.WakeLock wakeLock) {
        this.clientConnectionCallback = clientConnectionCallback;
        this.socket = socket;
        this.wakeLock = wakeLock;
    }

    private void log(String message, Object... objects) {
        clientConnectionCallback.logClient(threadId, message, objects);
    }

    @Override
    public void run() {
        this.threadId = Long.toString(Thread.currentThread().getId());

        if (wakeLock != null) {
            //TODO: remove
            log("wakelock acquire");
            wakeLock.acquire(650000);
        }

        HashMap<String, String> receivedRequest = null;
        byte[] response = new byte[0];

        BufferedReader bufferedReader;
        DataOutputStream dataOutputStream = null;

        try {
            this.socket.setSoTimeout(2000);
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            dataOutputStream = new DataOutputStream(socket.getOutputStream());

            while (running) {
                receivedRequest = receiveRequest(bufferedReader);
                if (receivedRequest.isEmpty()) {
                    running = false;
                    break;
                }

                response = prepareResponse(receivedRequest);

                dataOutputStream.write(response);
                dataOutputStream.flush();

                if (receivedRequest.containsKey("Connection") && receivedRequest.get("Connection").equals("close")) {
                    log("connection closed by client");
                    running = false;
                    break;
                }
            }

            log("thread finished");
        } catch (SocketException e) {
            String log = "Socket exception" + e.getMessage() + "\nreceivedRequest: " + receivedRequest + "\nresponse to str: " + new String(response);
            Log.w(TAG, log);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (dataOutputStream != null)
                    dataOutputStream.close();

                if (socket != null && !socket.isClosed())
                    socket.close();

                if (wakeLock != null) {
                    log("wakelock release");
                    wakeLock.release();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private HashMap<String, String> receiveRequest(BufferedReader bufferedReader) {
        HashMap<String, String> receivedRequest = new HashMap<>();

        while (true) {
            try {
                String line = bufferedReader.readLine();
                if (line == null) {
                    Log.i(TAG, threadId + " " + "line is null");
                    break;
                }

                if (line.isEmpty() || line.equals("\r\n")) {
                    Log.i(TAG, threadId + " " + "request received");
                    break;
                }

                if (line.startsWith("GET"))
                    receivedRequest.put(METHOD, "GET");

                String[] keyValuePair = line.split(" ", 2);
                String key = keyValuePair[0].replace(":", "");
                String value = keyValuePair[1];
                receivedRequest.put(key, value);

            } catch (SocketTimeoutException e) {
                if (Thread.currentThread().isInterrupted())
                    break;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return receivedRequest;
    }

    private byte[] fillHTMLTemplate(String code, String mime, String codeMessage, byte[] data) {
        byte[] returnBytes = null;

        int contentLength = (data == null) ? 0 : data.length;
        mime = (mime == null) ? HTML.MIME_TYPES.get(".html") : mime;

        String template = String.format(Locale.getDefault(), HTTP_RESPONSE_TEMPLATE, code, codeMessage, mime, contentLength);
        if (contentLength > 0) {
            ByteArrayOutputStream byteArrayOutputStream = null;
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                byteArrayOutputStream.write(template.getBytes());
                byteArrayOutputStream.write(data);

                returnBytes = byteArrayOutputStream.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (byteArrayOutputStream != null)
                        byteArrayOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            returnBytes = template.getBytes();
        }

        return returnBytes;
    }

    private String getMimeType(File file) {
        if (file == null)
            return null;

        String fileName = file.getName();
        String extension = fileName.substring(fileName.lastIndexOf("."));

        return HTML.MIME_TYPES.containsKey(extension) ? HTML.MIME_TYPES.get(extension) : null;
    }


    private byte[] prepareResponse(HashMap<String, String> request) {
        byte[] response;

        String method = request.get(METHOD);
        if (method == null)
            method = "NULL";

        switch (method) {
            case "GET": {
                String[] split = request.get("GET").split(" ");
                String path = split[0];
                String version = split[1];

                log("(%s) GET request: %s", socket.getInetAddress().getHostAddress(), path);

                if (path.contains("?")) {
                    String[] splitPath = path.split("\\?");
                    path = splitPath[0];
                    log("(%s) ignoring path after '?'", socket.getInetAddress().getHostAddress());
                }

                if (path.equals("/"))
                    path = path + "index.html";

                File file = loadFile(path);
                byte[] fileBytes = getFileBytes(file);

                if (fileBytes != null) {
                    String mime = getMimeType(file);
                    response = fillHTMLTemplate("200", mime, "OK", fileBytes);
                } else {
                    response = HTML.HTTP_RESPONSE_404.getBytes();
                }

                break;
            }

            case "POST":
            case "PUT":
            case "DELETE": {
                response = HTML.HTTP_RESPONSE_501.getBytes();
                break;
            }

            default:
                Log.i(TAG, threadId + " " + "unknown method: " + method);
                response = fillHTMLTemplate("404", null, "Not Found", "Nah".getBytes());

                break;
        }

        return response;
    }

    private byte[] getFileBytes(File file) {
        byte[] bytes = null;

        if (file != null) {
            byte[] fileBytes = new byte[(int) file.length()];

            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                int result = fileInputStream.read(fileBytes);
                if (result == -1) {
                    Log.i(TAG, threadId + " " + "file read result is -1");
                } else {
                    bytes = fileBytes;
                }
            } catch (FileNotFoundException e) {
                Log.i(TAG, threadId + " " + "requested file was not found: " + e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return bytes;
    }

    private File loadFile(String path) {
        if (path.startsWith("../")) {
            Log.i(TAG, threadId + " " + "'../' in path detected");
            return null;
        }

        String filePath = Environment.getExternalStorageDirectory() + "/" + HTML.HTML_FILES_PATH + path;
        Log.i(TAG, threadId + " " + "requested file " + filePath);

        return new File(filePath);
    }

    @Override
    public void interrupt() {
        super.interrupt();
        Log.i(TAG, "interrupted");
        running = false;

        try {
            if (socket != null && !socket.isClosed())
                socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
