package amadeo.httpserver2.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import amadeo.httpserver2.ConnectionCallback;
import amadeo.httpserver2.MainActivity;
import amadeo.httpserver2.R;
import amadeo.httpserver2.service.server.ServerThread;

/**
 * Created by Amadeo on 2018-08-25.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public class ForegroundService extends Service implements ConnectionCallback {
    private static final String TAG = "HTTPServer Service";

    private static final String MAIN_NOTIFICATION_CHANNEL = "http_server_main";

    private static final int MAIN_NOTIFICATION_ID = 1234;

    private Notification notification;

    public interface WorkCallback {
        void onStart(ForegroundService service);
    }

    private static final ArrayList<WorkCallback> workCallbacks = new ArrayList<>();
    private final static Lock mutex = new ReentrantLock(true);

    public ServerThread serverThread;

    public ConnectionCallback activityConnectionCallback = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "foreground service constructor");

        prepareNotification();
        startForeground(MAIN_NOTIFICATION_ID, notification);
    }

    private void prepareNotification() {
        Log.i(TAG, "creating notification");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mainNotificationChannel = new NotificationChannel(MAIN_NOTIFICATION_CHANNEL, getString(R.string.main_notification), NotificationManager.IMPORTANCE_MIN);
            mainNotificationChannel.setDescription(getString(R.string.main_notification_description));

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(mainNotificationChannel);
            }
        }

        Intent contentIntent = new Intent(this, MainActivity.class);
        contentIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        contentIntent.setAction("amadeo.intent.CONTENT");
        contentIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(this, 9, contentIntent, 0);

        Intent stopServiceIntent = new Intent(this, MainActivity.class);
        stopServiceIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        stopServiceIntent.setAction("amadeo.intent.STOP");
        PendingIntent stopServicePendingIntent = PendingIntent.getActivity(this, 0, stopServiceIntent, 0);

        notification = new NotificationCompat.Builder(this, MAIN_NOTIFICATION_CHANNEL)
                .setContentTitle(getString(R.string.content_title))
                .setContentText(getString(R.string.content_text))
                .setSmallIcon(R.drawable.ic_main_notification)
                .setContentIntent(contentPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setShowWhen(false)
                .addAction(0, getString(R.string.stop_application), stopServicePendingIntent)
                .build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mutex.lock();
        try {
            for (WorkCallback workCallback : workCallbacks) {
                workCallback.onStart(this);
            }
            workCallbacks.clear();
        } finally {
            mutex.unlock();
        }

        return START_STICKY;
    }

    public static void CreateWork(Context context, WorkCallback workCallback) {
        Thread thread = new Thread(() -> {
            if (workCallback != null) {
                mutex.lock();
                try {
                    workCallbacks.add(workCallback);
                } finally {
                    mutex.unlock();
                }
            }
            Intent intent = new Intent(context, ForegroundService.class);
            ContextCompat.startForegroundService(context, intent);
        });
        thread.start();
    }

    @Override
    public void onConnected() {
        if (activityConnectionCallback != null)
            activityConnectionCallback.onConnected();
    }

    @Override
    public void onDisconnected() {
        if (activityConnectionCallback != null)
            activityConnectionCallback.onDisconnected();
    }

    @Override
    public void log(String TAG, String message, Object... objects) {
        if (activityConnectionCallback != null)
            activityConnectionCallback.log(TAG, message, objects);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "foreground service destructor");

        if (serverThread != null && !serverThread.isInterrupted()) {
            try{
                serverThread.interrupt();
                serverThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
