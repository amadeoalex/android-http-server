package amadeo.httpserver2;

/**
 * Created by Amadeo on 2018-07-18.
 * "Welcome to the repository of terrible code,
 * I'll be your guide!"
 */
public interface ConnectionCallback {
    void onConnected();

    void onDisconnected();

    void log(String TAG, String message, Object... objects);
}
